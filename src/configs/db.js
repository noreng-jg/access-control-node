// import sqlite
const sqlite3 = require('sqlite3').verbose();
const { Pool, Client } = require('pg');

class DB {
  conn;
  dbname;

  constructor(dbtype, config) {
    this.dbname = dbtype;

    switch (dbtype) {
      case 'sqlite':
        this.conn = new sqlite3.Database(config.path);
        break;
      case 'postgres':
        this.conn = new Client(config);
        this.connect();
        break;
      default:
        throw new Error('DB type not supported');
    }
  }

  // postgres case
  async connect() {
    try {
      await this.conn.connect();
    } catch (err) {
      console.log(err.message);
    }
  }
}
// module.exports = new sqlite3.Database('./test.db');
module.exports  = DB;
