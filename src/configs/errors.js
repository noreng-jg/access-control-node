class Errors extends Error {
  constructor(message, statusCode) {
    super(message);
    this.statusCode = statusCode;
    this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error';
    this.isOperational = true;

    Error.captureStackTrace(this, this.constructor);
  }

  static badRequest(message) {
    return new this(message, 400);
  }

  static unauthorized(message) {
    return new this(message, 401);
  }

  static forbidden(message) {
    return new this(message, 403);
  }

  static notFound(message) {
    return new this(message, 404);
  }

  static conflict(message) {
    return new this(message, 409);
  }

  static internalServerError(message) {
    return new this(message, 500);
  }

  static notImplemented(message) {
    return new this(message, 501);
  }

  static badGateway(message) {
    return new this(message, 502);
  }

  static serviceUnavailable(message) {
    return new this(message, 503);
  }

  static gatewayTimeout(message) {
    return new this(message, 504);
  }

  static getMessage() {
    return this.message;
  }

  // error handler
  static Handler(err, res) {
    const { statusCode, status, message } = err;
    const error = {
      message,
    };

    res.status(statusCode).json(error);
  }
}

module.exports = Errors;
/*
const InvalidEmailOrUsername = new Error('Invalid email or username');
const InvalidEmailFormat = new Error('Invalid email format');
const EmailAlreadyExists = new Error('Email already exists');
const UsernameAlreadyExists = new Error('Username already exists');

const Handler = (err, res) => {
  switch (err) {
    case InvalidEmailOrUsername:
      return res.status(400).json({
        message: 'Invalid email or username',
      });
    case InvalidEmailFormat:
      return res.status(400).json({
        message: 'Invalid email format',
      });
    case EmailAlreadyExists:
      return res.status(409).json({
        message: 'Email already exists',
      });
    case UsernameAlreadyExists:
      return res.status(409).json({
        message: 'Username already exists',
      });
    default:
      return res.status(500).json({
        message: 'Internal server error',
      });
  }
};
*/
