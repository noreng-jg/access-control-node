class Envs {
  jwtSecret = process.env.JWT_SECRET || '0a6b944d-d2fb-46fc-a85e-0295c986cd9f';
  // load postgres environment variables
  postgresUser = process.env.POSTGRES_USER || 'postgres';
  postgresPassword = process.env.POSTGRES_PASSWORD || 'postgres';
  postgresHost = process.env.POSTGRES_HOST || 'localhost';
  postgresPort = process.env.POSTGRES_PORT || 5432;
  postgresDatabase = process.env.POSTGRES_DB || 'postgres';
  uploadPath = process.env.UPLOAD_PATH || './uploads';
  // admin user
  adminUsername = process.env.ADMIN_USERNAME || 'admin';
  adminPassword = process.env.ADMIN_PASSWORD || 'admin';
}

module.exports = new Envs();
