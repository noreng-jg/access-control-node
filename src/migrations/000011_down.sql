-- rollback add unique field constraint to table license
ALTER TABLE licenses
DROP CONSTRAINT license_requirement_id_fkey;

-- rollback add column requirement_id
ALTER TABLE licenses
DROP COLUMN requirement_id;
