-- set the field user_id of licenses table to nullable
--
-- ALTER TABLE licenses DROP CONSTRAINT user_id;
ALTER TABLE licenses
DROP COLUMN user_id;

-- change the field name authenticated from the users table to last_requirement_accepted
--
ALTER TABLE users
RENAME COLUMN authenticated TO last_requirement_accepted;
