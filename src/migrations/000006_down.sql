-- remove user_id from the info table --
ALTER TABLE info DROP COLUMN user_id;

-- drop the info table --
DROP TABLE info;
