-- change the requirement_id type in the requirements table to an varchar(18)
--
ALTER TABLE requirements
DROP COLUMN requirement_id;

ALTER TABLE requirements

ADD COLUMN requirement_id varchar(18) NOT NULL;

ALTER TABLE requirements ADD PRIMARY KEY (requirement_id);
