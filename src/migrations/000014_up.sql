-- rename the last_requirement_accepted in users table to has_active_license
ALTER TABLE users
RENAME COLUMN last_requirement_accepted TO has_active_license;
