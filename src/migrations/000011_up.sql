-- add unique field constraint to table license
-- A license will only exists if a requirement is fulfilled
--
ALTER TABLE licenses
ADD COLUMN requirement_id varchar(18) NOT NULL;

-- add foreign key to requirement_id
ALTER TABLE licenses
ADD CONSTRAINT license_requirement_id_fkey FOREIGN KEY (requirement_id) REFERENCES requirements(requirement_id);
