-- add unique constraint to table info user_id
--
ALTER TABLE info
ADD CONSTRAINT uq_user_id UNIQUE(user_id);
