-- add column for intended_months to requirements table
ALTER TABLE requirements ADD COLUMN intended_months INTEGER NOT NULL;
