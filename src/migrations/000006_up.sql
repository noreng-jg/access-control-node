-- Create an info field which makes a one to one relation from info table to the users table
--
CREATE TABLE if not exists INFO (
    info_id varchar(18) UNIQUE PRIMARY KEY NOT NULL,
    user_id varchar(18) NOT NULL,
    business_name varchar(255) NOT NULL,
    address varchar(255) NOT NULL,
    city varchar(255) NOT NULL,
    state varchar(255) NOT NULL,
    country varchar(255) NOT NULL,
    phone varchar(20) NOT NULL,
    reason varchar(255) NOT NULL,
    optional_medias varchar(255),
    description text NOT NULL
);

ALTER TABLE info
ADD CONSTRAINT info_user_id_fk FOREIGN KEY (user_id) REFERENCES users(id);
