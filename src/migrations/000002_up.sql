--- this migration will create a license table and 
--- will bind a foreign key in the users table to it
--- it will also be addeded a new authenticated field to the users
--- table
CREATE TABLE IF NOT EXISTS licenses (
  license_id varchar(18) NOT NULL,
  expires_at timestamp NOT NULL,
  disk_space_mb int NOT NULL,
  user_id varchar(18) NOT NULL,
  FOREIGN KEY (user_id) REFERENCES users(id),
  PRIMARY KEY (license_id)
);

ALTER TABLE USERS
ADD COLUMN authenticated boolean NOT NULL DEFAULT false;
