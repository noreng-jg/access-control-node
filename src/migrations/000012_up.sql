-- drop constraint fk_licenses_users
ALTER TABLE licenses
DROP CONSTRAINT fk_licenses_users;

-- drop column user_id in licenses
ALTER TABLE licenses
DROP COLUMN user_id;

-- define the user_id column in licenses as a varchar(18) not nullable
ALTER TABLE licenses
ADD COLUMN user_id varchar(18) NOT NULL;

-- add a foreign key constraint to the user_id column in licenses
ALTER TABLE licenses
ADD CONSTRAINT fk_licenses_users
FOREIGN KEY (user_id)
REFERENCES users (id);
