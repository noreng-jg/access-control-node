-- rollback rename the last_requirement_accepted in users table to has_active_license
ALTER TABLE users RENAME COLUMN has_active_license TO last_requirement_accepted;
