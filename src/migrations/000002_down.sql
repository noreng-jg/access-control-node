-- drops table license and removes the fields license_id and authenticated from users
ALTER TABLE users DROP COLUMN authenticated;
ALTER TABLE users DROP COLUMN license_id;
DROP TABLE IF EXISTS liceses;
