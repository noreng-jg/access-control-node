-- rollback make user_id field not nullable in licenses table
ALTER TABLE licenses
DROP CONSTRAINT fk_licenses_users;

ALTER TABLE licenses
DROP COLUMN user_id;

ALTER TABLE licenses
ADD COLUMN user_id varchar(18) NULL;

ALTER TABLE licenses
ADD CONSTRAINT fk_licenses_users
FOREIGN KEY (user_id)
REFERENCES users (id);
