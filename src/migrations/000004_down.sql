-- add the user_id field to the licenses table
-- and add a foreign key constraint to the user_id field
--

-- This might fail
ALTER TABLE licenses
ADD COLUMN user_id varchar(18) NOT NULL;
ADD CONSTRAINT licenses_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


-- rename the last_requirement_accepted in users table to authenticated
ALTER TABLE users
RENAME COLUMN last_requirement_accepted TO authenticated;
