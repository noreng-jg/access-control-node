-- add a foreign key to licenses in the users table
-- so that an user might have multiple licenses
ALTER TABLE licenses
ADD COLUMN user_id varchar(18);

ALTER TABLE licenses
ADD CONSTRAINT fk_licenses_users FOREIGN KEY (user_id) REFERENCES users(id);
-- --------------------------------------------------------
-- create a table to requirements
-- --------------------------------------------------------
CREATE TABLE IF NOT EXISTS requirements (
  requirement_id INTEGER PRIMARY KEY,
  status VARCHAR(255) NOT NULL CHECK (status IN ('accepted', 'rejected', 'pending')),
  name VARCHAR(255) NOT NULL,
  description TEXT,
  user_id VARCHAR(18) NOT NULL,
  usage INT NOT NULL,
  created_at timestamp NOT NULL,
  finalized_at timestamp NULL
);


-- modify users table so that it can have multiple requirements
-- --------------------------------------------------------

ALTER TABLE requirements
ADD CONSTRAINT fk_requirements_users FOREIGN KEY (user_id) REFERENCES users(id);
