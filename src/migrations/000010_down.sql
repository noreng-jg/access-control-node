-- change the requirement_id type in the requirements table to an integer 
--
ALTER TABLE requirements
DROP COLUMN requirement_id;

ALTER TABLE requirements
ADD COLUMN requirement_id INTEGER NOT NULL;

ALTER TABLE requirements ADD PRIMARY KEY (requirement_id);
