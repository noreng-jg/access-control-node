const sleep = (ms) => new Promise(resolve => setTimeout(resolve, ms));

async function worker(time, logger, ucs) {
  /*
   * This is the worker function that will be called by the master process.

   * It will be used to continually check the users that have an expired license.
   */

  while (true) {
    logger.info('worker running...');
    logger.trace(ucs);
    await sleep(time*60*1000);
  }
}

module.exports = { worker };
