const db = require('./configs/db');
const repository = require('./repositories/repository');
const ucs = require('./use-cases/use-cases');
const controllers = require('./controllers/controller');
const routes = require('./routes/app');

const envs = require('./configs/envs');
const errors = require('./configs/errors');
const logger = require('./configs/logger');

const { worker } = require('./worker');

logger.debug('Connecting to db...');

const pgDb = new db('postgres', {
  user: envs.postgresUser,
  password: envs.postgresPassword,
  host: 'localhost',
  port: envs.postgresPort,
  database: envs.postgresDatabase
});

const repo = new repository(pgDb.conn, logger, errors);
const uc = new ucs(repo, logger, errors);
const ctrl = new controllers(envs.jwtSecret, uc, logger, errors);

// run worker to process jobs every 10 minutes
worker(10, logger, ucs);

// initialize app
app = new routes(3000, ctrl);

// start app
app.init();
