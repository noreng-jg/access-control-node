const express = require('express');
const router = express.Router();
const mid = require('./middlewares/middleware');
const tokenUtils = require('../utils/token');
const envs = require('../configs/envs');
const fs = require('fs');

function wavRoutes() {
  router.get('/', mid.authenticationMiddleware, async (req, res) => {
    let userId;

    try {
      const token = mid.getToken(req);
      userId = tokenUtils.getUserIdFromToken(token)
    } catch (err) {
      res.status(500).json({ message: err.message });
    }

    // fs show the files of current folder

    // checks if a folder with the userId name exists in media root
    // if not, creates it
    try {
      const mediaRoot = envs.uploadPath;
      const userFolder = mediaRoot + userId;
      if (!fs.existsSync(userFolder)) {
        fs.mkdirSync(userFolder);
      }
    } catch (err) {
      res.status(500).json({ message: err.message });
    }

    res.status(201).json({ message: 'wav route' });
  });

  return router;
}

module.exports = { wavRoutes };
