const express = require('express');
const router = express.Router();
const mid = require('./middlewares/middleware');

function userRoutes(userController) {
  router.post('/', userController.createUser);
  router.post('/info', mid.authenticationMiddleware, userController.addUserInfo);
  router.post('/requirements', mid.authenticationMiddleware, userController.addRequirements);
  router.patch('/authorize', mid.adminAuthenticationMiddleware, (req, res) => {
    res.status(200).json({ message: 'Authoring user' });
  })
  return router;
}

module.exports = { userRoutes };
