const express = require('express');
const router = express.Router();
const mid = require('./middlewares/middleware');

function adminRoutes(adminController) {
  router.post('/login', adminController.login);
  router.post('/license', mid.adminAuthenticationMiddleware, adminController.license);
  return router;
}

module.exports = { adminRoutes };
