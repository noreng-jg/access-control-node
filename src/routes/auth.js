const express = require('express');
const router = express.Router();

function authRoutes(authController) {
  router.post('/login', authController.login);
  return router;
}

module.exports = { authRoutes };
