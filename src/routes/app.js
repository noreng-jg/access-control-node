const express = require('express');
const { userRoutes } = require('./user');
const { authRoutes } = require('./auth');
// const { wavRoutes } = require('./wav');
const { adminRoutes } = require('./admin');

class App {
  constructor(bindPort, controllers) {
      this.controllers = controllers;
      this.bindPort = bindPort;
  }

  init() {
    const app = express();
    app.use(express.json());

    // app.use('/wav', wavRoutes()); I will have to work on this later
    app.use('/users', userRoutes(this.controllers.users));
    app.use('/auth',  authRoutes(this.controllers.auth));
    app.use('/admin', adminRoutes(this.controllers.admin));

    app.listen(this.bindPort || 3000, () => {
      console.log(`Server listening on port ${this.bindPort}`);
    });
  }
};

module.exports = App;
