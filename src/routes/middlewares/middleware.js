const envs = require('../../configs/envs');
const jwt = require('jsonwebtoken');

class Middlewares {
  constructor() {
    this.jwtSecret = envs.jwtSecret;

    this.authenticationMiddleware = this.authenticationMiddleware.bind(this);
    this.adminAuthenticationMiddleware = this.adminAuthenticationMiddleware.bind(this);
    this.getToken = this.getToken.bind(this);
  }

  authenticationMiddleware(req, res, next) {
    const token = this.getToken(req);

    if (token !== null) {
      jwt.verify(token, this.jwtSecret, (err, user) => {
        if (err) {
          res.status(401).send({ error: 'Invalid token' });
        } else {
          req.user = user;
          next();
        }
      });
    } else {
      res.status(401).send({ error: 'No token provided' });
    }
  }

  adminAuthenticationMiddleware(req, res, next) {
    const token = this.getToken(req);

    if (token !== null) {
      jwt.verify(token, this.jwtSecret, (err, user) => {
        if (err) {
          res.status(401).send({ error: 'Invalid token' });
        } else if (user.role !== 'admin') {
          res.status(401).send({ error: 'You are not authorized' });
        } else {
          req.user = user;
          next();
        }
      });
    } else {
      res.status(401).send({ error: 'No token provided' });
    }
  }

  getToken(req) {
    const authHeader = req.headers.authorization;

    if (authHeader) {
      const token = authHeader.split(' ')[1];

      return token;
    }

    return null;
  }
};

module.exports = new Middlewares();
