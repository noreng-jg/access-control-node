const express = require('express');
const router = express.Router();
const mid = require('./middlewares/middleware');

function utilRouter() {
  router.get('/', mid.authenticationMiddleware, function(req, res) {
    res.send('Utils');
  });

  return router;
}

module.exports = { utilRouter };
