class RequirementRepository {
  conn;
  constructor(conn, logger, errors) {
    this.conn = conn;
    this.logger = logger;
    this.errors = errors;
  }

  async getRequirementById(id) {
    this.logger.trace(`requirement repository - Getting requirement with id ${id}`);
    const sql = `SELECT * FROM requirements WHERE requirement_id = $1`;
    const params = [id];

    try {
      const requirement = await this.conn.query(sql, params);
      return requirement.rows[0];
    } catch (error) {
      this.logger.error(error);
      throw this.errors.internalServerError();
    }
  }
};

module.exports = RequirementRepository;
