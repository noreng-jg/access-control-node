// user class repository that receives a conn object to database
const fields = require('../utils/fields/fields');
const gen = require('../utils/generator');

class UserRepository {
  conn;
  logger;
  constructor(conn, logger, errors) {
    this.conn = conn;
    this.logger = logger;
    this.errors = errors;
  }

  async addRequirements(userData, userId) {
    this.logger.trace(`add user requirements repository: ${JSON.stringify(userData)}`);
    return userData;
  }

  async addInfo(userData, userId) {
    this.logger.trace(`add user info: ${userData}`);
    let params = [...fields.info.required, ...fields.info.optional];
    const values = params.map(field => userData[field]);

    this.logger.trace('in user repository add info');
    this.logger.trace(params);

    const info_id = gen.uuid(18);

    const sql = `INSERT INTO info (info_id, user_id, ${params.join(', ')}) VALUES ($1, $2, ${params.map((_, i) => '$' + (i+3)).join(', ')})`;

    const result = await this.conn.query(sql, [info_id, userId, ...values]);
    
    return result;
  }

  // create a new user
  async createUser(user) {
    this.logger.trace(`creating user: ${JSON.stringify(user)}`);

    const {
      username,
      email,
      password,
      created_at,
      updated_at,
    } = user;

    const userId = gen.uuid(18);
    const hashedPassword = await gen.hash(password);

    const sql = `INSERT INTO users (id, username, email, password, created_at, updated_at) VALUES ($1, $2, $3, $4, $5, $6)`;
    const params = [userId, username, email, hashedPassword, created_at, updated_at];

    await this.conn.query(sql, params);
  }

  // get user by id
  async getUserById(id) {
    const sql = `SELECT * FROM users WHERE id = $1`;
    const params = [id];
    const { rows } = await this.conn.query(sql, params);
    return rows[0];
  }

  // get user by email
  async getUserByEmail(email) {
    this.logger.trace(`get user by email: ${email}`);

    const sql = `SELECT * FROM users WHERE email = $1`;
    const params = [email];

    const { rows } = await this.conn.query(sql, params);

    return rows[0];
  }

  // get user by username
  async getUserByUsername(username) {
    this.logger.trace(`get user by username: ${username}`);

    const sql = `SELECT * FROM users WHERE username = $1`;
    const params = [username];

    const { rows } = await this.conn.query(sql, params);

    return rows[0];
  }


  async addUserRequirements(userData, userId) {
    this.logger.trace(`add user requirements: ${JSON.stringify(userData)}`);
    this.logger.debug(`userData, userId: ${JSON.stringify(userData)}, ${userId}`);

    const params = [...fields.requirements.required, ...fields.requirements.optional];

    const values = params.map(field => userData[field]);

    const requirements_id = gen.uuid(18);

    const sql = `INSERT INTO requirements (requirement_id, user_id, ${params.join(', ')}) VALUES ($1, $2, ${params.map((_, i) => '$' + (i+3)).join(', ')})`;

    try {
      const result = await this.conn.query(sql, [requirements_id, userId, ...values]);
      return result[0];
    } catch (err) {
      this.logger.error(err);
      throw this.errors.internalServerError(err.message);
    }
  }


  async getUserInfo(userId) {
    this.logger.trace(`get user info: ${userId}`);

    const sql = `SELECT * FROM info WHERE user_id = $1`;

    const params = [userId];

    const { rows } = await this.conn.query(sql, params);

    return rows[0];
  }
};

module.exports = UserRepository;
