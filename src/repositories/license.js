const fields = require('../utils/fields/fields');
const gen = require('../utils/generator');

class LicenseRepository {
  constructor(conn, logger, error) {
    this.conn = conn;
    this.logger = logger;
    this.error = error;
  }

  async createLicense(license, data) {
    this.logger.trace('license.createLicense repository');

    this.logger.debug('license.createLicense repository - licenseData: ', JSON.stringify(license));
    this.logger.debug('license.createLicense repository - requirementData: ', JSON.stringify(data));
    this.logger.debug(data);
    this.logger.debug(license);

    const {
      userHasActiveLicense,
      requirementStatus,
    } = data;

    const {
      user_id,
      requirement_id
    } = license;

    const params = [ ...fields.license.required, ...fields.license.optional ];

    const licenseValues = params.map((param) => license[param]);

    const licenseId = gen.uuid(18);

    const sqls = [
      {
        // sql 1
        // add license values to licenses table
        query: `INSERT INTO licenses ( license_id, user_id, requirement_id, ${params.join(', ')}) VALUES ( $1, $2, $3, ${params.map((_, i) => '$' + (i + 4)).join(', ')})`,
        params: [ licenseId, user_id, requirement_id, ...licenseValues ],
      },
      {
        // sql 2
        // update field status in table requirements
        query: `UPDATE users SET has_active_license = $1 WHERE id = $2`,
        params: [ userHasActiveLicense, user_id ],
      },
      {
        // sql 3
        // update has_active_license in users table
        query: `UPDATE requirements SET status = $1 WHERE requirement_id = $2`,
        params: [ requirementStatus, requirement_id ],
      }
    ];

    try {
      for (let i = 0; i < sqls.length; i++) {
        await this.conn.query(sqls[i].query, sqls[i].params);
      }

      this.logger.debug('license.createLicense repository - result: ');
    } catch (err) {
      console.log(err.message);
      this.logger.error(err.message);
      throw this.error.internalServerError(err.message);
    }
  }
};

module.exports = LicenseRepository;
