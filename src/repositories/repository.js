const UserRepository = require('./user');
const LicenseRepository = require('./license');
const RequirementRepository = require('./requirements');

class Repository {
  constructor(db, logger, errors) {
    this.db = db;
    this.users = new UserRepository(db, logger, errors);
    this.requirements = new RequirementRepository(db, logger, errors);
    this.licenses = new LicenseRepository(db, logger, errors);
  }
}

module.exports = Repository;
