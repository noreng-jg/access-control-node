const fields = require('./fields/fields');

class Validators {
  constructor() {
    this.fieldNames = [...Reflect.ownKeys(fields).map((r)=>r)];
  }

  isValidPhoneNumber(phoneNumber) {
    return /^\d{10}$/.test(phoneNumber);
  }

  fieldValuesNotEmpty(object, fieldName) {
    if (this.fieldNames.includes(fieldName)) {
      for (let key of fields[fieldName].required) {
        if (object[key] === undefined || object[key] === '') {
          return false;
        }
      }

      return true;
    }

    return false;
  }
};

module.exports = new Validators();
