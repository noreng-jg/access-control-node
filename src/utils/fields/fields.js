const fields = {
  requirements: {
    required: [
      'name',
      'usage',
      'intended_months',
      'created_at',
      'status',
    ],
    optional: [
      'description',
    ],
  },
  license: {
    required: [
      'created_at',
      'price',
      'active',
    ],
    optional: [
      'expires_at',
      'disk_space_mb',
    ],
  },
  info: {
    required: [
      'city',
      'country',
      'reason',
      'state',
      'description',
      'business_name',
      'address',
      'phone',
    ],
    optional: [
      'optional_medias',
    ],
  },
}

module.exports = fields;
