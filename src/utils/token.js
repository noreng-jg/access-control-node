const jwt = require('jsonwebtoken');
const config = require('../configs/envs');
const errors = require('../configs/errors');

class Token {
  constructor() {
    this.userIdDFromContext = this.userIdDFromContext.bind(this);
  }

  static userIdDFromContext(req, res) {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      throw errors.unauthorized('Token is missing');
    }

    const token = authHeader.split(' ')[1];

    try {
      const decoded = jwt.verify(token, config.jwtSecret);
      return decoded.id;
    } catch (err) {
      throw errors.unauthorized('Token is invalid');
    }
  }
};

module.exports = Token;
