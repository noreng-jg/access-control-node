const crypto = require('crypto');
const bcrypt = require('bcrypt');

class Generator {
  static uuid(length) {
    return crypto.randomBytes(length/2).toString('hex');
  }

  static async hash(value) {
    const randomness = 10;
    const hash = await bcrypt.hash(value, randomness);
    return hash;
  }
};

module.exports = Generator;
