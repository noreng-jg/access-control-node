const jwt = require('jsonwebtoken');

class AuthController {
  constructor(jwtSecret, authUseCase, logger, errors) {
    this.jwtSecret = jwtSecret;
    this.authUseCase = authUseCase;
    this.logger = logger;
    this.errors = errors;
    this.expiration = '12h';

    this.login = this.login.bind(this);
  }

  async login(req, res) {
    const { email, username, password } = req.body;

    try {
      const user = await this.authUseCase.login(email, username, password);
      const token = jwt.sign({ id: user.id, username: user.username }, this.jwtSecret, { expiresIn: this.expiration });

      res.json({
        token,
        'username': user.username,
        'email': user.email,
      });
    } catch (error) {
      res.status(401).json({
        message: error.message,
      });
    }
  }
};

module.exports = AuthController;
