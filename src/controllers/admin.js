const envs = require('../configs/envs');
const jwt = require('jsonwebtoken');

class AdminController {
    constructor(adminUseCase, logger, errors) {
        this.envs = envs;
        this.jwtSecret = envs.jwtSecret;
        this.adminUseCase = adminUseCase;
        this.logger = logger;
        this.errors = errors;

        this.expiration = '4h';
        this.login = this.login.bind(this);
        this.license = this.license.bind(this);
    }

    async license(req, res) {
      const data = req.body;
      const created_at = new Date();

      this.logger.trace('In admin license')

      try {
        const license = await this.adminUseCase.addLicense({ ...data, created_at });

        res.status(200).json({
          license: 'This is a license',
          data: license,
        })
      } catch (error) {
        this.errors.Handler(error, res);
      }
    }

    async login(req, res) {
      const { username, password } = req.body;

      this.logger.trace('In admin login')

      try {
        if (username === envs.adminUsername && password === envs.adminPassword) {
          const token = jwt.sign({ username, role: 'admin' }, this.jwtSecret, { expiresIn: this.expiration });
        res.json({
          token,
          'username': username,
        });   
        } else {
          res.status(401).json({
            message: 'Invalid username or password',
          });
        }
      } catch (error) {
        res.status(401).json({
          message: error.message,
        });
      }
    }
};

module.exports = AdminController;
