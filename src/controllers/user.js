const tokenUtils = require('../utils/token');

class UserController {
  constructor(userUseCase, logger, errors) {
    this.userUseCase = userUseCase;
    this.logger = logger;
    this.errors = errors;

    // high order function need to be binded
    // epic answer https://stackoverflow.com/a/59060545/13575000
    this.createUser = this.createUser.bind(this);
    this.addUserInfo = this.addUserInfo.bind(this);
    this.addRequirements = this.addRequirements.bind(this);
  }

  async addRequirements(req, res) {
    this.logger.trace('addRequirements called in user controller');

    try {
      const userId = tokenUtils.userIdDFromContext(req, res)

      const created_at = new Date();
      const message = await this.userUseCase.addRequirements({ ...req.body, created_at }, userId);
      // new date now
      this.logger.trace('addRequirements finished in user controller');
      return res.status(200).json({ message });
    } catch (err) {
      this.logger.error(err);
      this.errors.Handler(err, res);
    }
  }

  async addUserInfo(req, res) {
    try {
      const userId = tokenUtils.userIdDFromContext(req, res)
      const data = req.body;

      await this.userUseCase.addInfo(data, userId);
      return res.status(200).json({ message: 'User info added', data });
    } catch (err) {
      this.logger.error(err);
      this.errors.Handler(err, res);
    }
  }

  async createUser(req, res)  {
      const { username, password, email } = req.body;

      const created_at = new Date();
      const updated_at = new Date();

      const user = { username, password, email, created_at, updated_at };

      try {
        await this.userUseCase.createUser(user);
        return res.status(201).json({ message: 'User created', user: { username, email } });
      } catch (err) {
        this.errors.Handler(err, res);
      }
  }
};

module.exports = UserController;
