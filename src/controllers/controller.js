const UserController = require('./user');
const AuthController = require('./auth');
const AdminController = require('./admin');

class Controller {
  constructor(jwtSecret, ucs, logger, errors) {
    this.users = new UserController(ucs.users, logger, errors);
    this.auth = new AuthController(jwtSecret, ucs.auth, logger, errors);
    this.admin = new AdminController(ucs.admin, logger, errors);
  } 
}

module.exports = Controller;
