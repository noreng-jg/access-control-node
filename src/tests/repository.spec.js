const repository = require('../repositories/repository');
const logger = require('../configs/logger');
const errors = require('../configs/errors');
const gen = require('../utils/generator');

describe('Repository', () => {
  let Repository;
  let conn;

  beforeAll(() => {
    gen.uuid = jest.fn().mockImplementation(() => 'id');
    conn = {
      query: jest.fn().mockImplementation((query, params, callback) => {
    })
    };

    Repository = new repository(conn, logger, errors);
  });

  it('Repository', () => {
    expect(Repository).toBeDefined();
  });

  describe('UserRepository', () => {
    // before all tests
    beforeEach(() => {
      conn.query.mockClear();
    });

    it('should be defined', () => {
      expect(Repository.users).toBeDefined();
    });

    it('should addInfo to user', () => {
      const userData = {
        city: 'test',
        country: 'test',
        reason: 'test',
        state: 'test',
        description: 'test',
        business_name: 'test',
        address: 'test',
        phone: 'test',
        optional_medias: '',
      }

      const userId = 'id';
      const infoId = 'id';
      
      Repository.users.addInfo(userData, userId);

      expect(conn.query).toHaveBeenCalledWith('INSERT INTO info (info_id, user_id, city, country, reason, state, description, business_name, address, phone, optional_medias) VALUES ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11)', [infoId, userId, userData.city, userData.country, userData.reason, userData.state, userData.description, userData.business_name, userData.address, userData.phone, userData.optional_medias]);
    }
  );

    it('should add user requirements', () => {
      const userData = {
        'name': 'test',
        'usage': '1902',
        'intended_months': '12',
        'created_at': '2019-02-01',
        'status': 'pending',
        'user_id': 'id',
        'description': '',
      }

      const userId = 'id';
      const infoId = 'id';

      Repository.users.addUserRequirements(userData, userId);

      // clean conn query mock

      // to have been called once
      expect(conn.query).toHaveBeenCalledWith('INSERT INTO requirements (requirement_id, user_id, name, usage, intended_months, created_at, status, description) VALUES ($1, $2, $3, $4, $5, $6, $7, $8)', [infoId, userData.user_id, userData.name, userData.usage, userData.intended_months, userData.created_at, userData.status, userData.description]);
    })
  });

  describe('LicenseRepository', () => {
    // before all tests
    beforeEach(() => {
      conn.query.mockClear();
    });

    it('should be defined', () => {
      expect(Repository.licenses).toBeDefined();
    });
  });
});
