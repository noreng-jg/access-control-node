const usecases = require('../use-cases/use-cases');
const logger = require('../configs/logger');
const errors = require('../configs/errors');

describe('UseCases', () => {
  let UseCases;
  let mockRepository;
  
  beforeAll(() => {
    mockRepository = {
      users: {
        getUserById: jest.fn(),
      },
      licenses: {
        createLicense: jest.fn(),
      },
      requirements: {
        getRequirementById: jest.fn(), 
      },
    };

    UseCases = new usecases(mockRepository, logger, errors);
  });

  it('UseCase is defined', () => {
    expect(UseCases).toBeDefined();
  });
});
