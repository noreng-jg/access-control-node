const userUseCase = require('./user');
const authUseCase = require('./auth');
const adminUseCase = require('./admin');

class UseCases {
  constructor(repository, logger, errors) {
    this.users = new userUseCase(repository.users, logger, errors);
    this.auth =  new authUseCase(repository.users, logger, errors);
    this.admin = new adminUseCase(repository, logger, errors);
  }
};

module.exports = UseCases;
