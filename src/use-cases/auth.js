const bcrypt = require('bcrypt');

class AuthUseCase {
  constructor(userRepository, logger, errors) {
    this.userRepository = userRepository;
    this.logger = logger;
    this.errors = errors;
  }

  async login(email, username, password) {
    let user;

    try {
      user = await this.userRepository.getUserByEmail(email);
    } catch (error) {
      this.logger.error(error);
      throw this.errors.internalServerError('Internal server error');
    }

    if (user === undefined) {
      try {
        user = await this.userRepository.getUserByUsername(username);
      } catch (error) {
        this.logger.error(error);
        throw this.errors.internalServerError('Internal server error');
      }
    }

    if (user === undefined) {
      throw this.errors.notFound('User not found');
    }

    this.logger.trace('the passwords are', password, user.password);

    if (!bcrypt.compareSync(password, user.password)) {
      throw this.errors.unauthorized('Invalid password');
    }

    return user;
    }
};

module.exports = AuthUseCase;
