const validator = require('../utils/validators');

class AdminUseCase {
  constructor(repository, logger, errors) {
    this.repository = repository;
    this.logger = logger;
    this.errors = errors;

  }

  async addLicense(data) {
    this.logger.debug('addLicense', data);
    this.logger.trace('addLicense in admin use-case');

    const { 
      created_at: createdAt,
      requirement_id: requirementId,
      disk_space_mb: diskSpaceMb,
      expiration_months: expirationMonths,
      active
    } = data;

    try {

      const check = validator.fieldValuesNotEmpty(data, 'license')

      this.logger.debug(check);
      
      if (!check) {
        throw this.errors.badRequest('Some fields are missing');
      }

      const { 
        requirements: reqRepository,
        licenses: licRepository,
        users: userRepository
      } = this.repository;

      const requirement = await reqRepository.getRequirementById(requirementId);
      if (!requirement) {
        throw this.errors.notFound('Requirement not found');
      }

      const user = await userRepository.getUserById(requirement.user_id);

      this.logger.debug(user);
      this.logger.debug('calling the user instance in admin use case');

      if (user.has_active_license) {
        throw this.errors.forbidden('Action forbidden, user already has a license vinculated');
      }

      if (requirement.status !== 'pending') {
        throw this.errors.badRequest('Requirement is not pending');
      }

      let expiration = new Date();
      
      if (!expirationMonths) {
        console.log('expiresAt is null', expirationMonths);
        expiration = expiration.setMonth(createdAt.getMonth() + requirement.intended_months);
      } else {
        expiration = expiration.setMonth(createdAt.getMonth() + parseInt(expirationMonths, 10));
      }

      let usageMb = 0;

      if (diskSpaceMb) {
        usageMb = diskSpaceMb;
      } else {
        usageMb = requirement.usage;
      }

      const license = {
        created_at: createdAt,
        requirement_id: requirementId,
        expires_at: new Date(expiration),
        disk_space_mb: usageMb,
        user_id: requirement.user_id,
        active,
      };

      const metadata = {
        userHasActiveLicense: (active) ? 'true' : 'false',
        requirementStatus: (active) ? 'accepted' : 'rejected',
      }

      // in the license repository pass license object and
      // the maped active field to user has_active_license and
      // requirement status to rejected or accepted
      
      await licRepository.createLicense(license, metadata);

      return { license, metadata };
    } catch (error) {
      this.logger.error('addLicense', error.message);
      throw error;
    }
  }
};

module.exports = AdminUseCase;
