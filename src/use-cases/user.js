const validators = require('../utils/validators');

class UserUseCase {
  pendingStatus = 'pending';

  constructor(userRepository, logger, errors) {
    this.userRepository = userRepository;
    this.logger = logger;
    this.errors = errors;
  }

  async addRequirements(userData, userId) {
    this.logger.debug('addRequirements in users use-case');
    this.logger.debug(userData);
    // check that the user has info 
    // use the userId and status pending when creating a new requirement
    try {
      const userInfo = await this.userRepository.getUserInfo(userId);

      if (!userInfo) {
        this.logger.error('user info not found');
        throw this.errors.internalServerError('user info not found');
      }

      const data = { ...userData, status: this.pendingStatus };

      this.logger.debug(data);

      const check = validators.fieldValuesNotEmpty(data, 'requirements');

      if (!check) {
        this.logger.error('missing required fields');
        throw this.errors.badRequest('missing required fields');
      }

      const requirement = await this.userRepository.addUserRequirements(data, userId);

      this.logger.debug(requirement);
      return requirement;
    } catch (error) {
      this.logger.error(error);
      throw error;
    }
  }

  async addInfo(userData, userId) {
    this.logger.debug('addInfo');
    this.logger.debug(userData);

    const check = validators.fieldValuesNotEmpty(userData, 'info');

    if (!check) {
      this.logger.trace('Some fields are empty');
      throw this.errors.badRequest('Some fields are empty');
    }

    // call repository to create info to user

    try {
      const info = await this.userRepository.addInfo(userData, userId);

      return info;
    } catch (error) {
      this.logger.error(error);
      throw this.errors.internalServerError('Internal server error');
    }
  }

  async createUser(userData) {
    const { username, email } = userData;

    // validate username
    if (!username || !email) {
      this.logger.trace('username or email is missing');
      throw this.errors.badRequest('username or email is missing');
    }

    // validate email
    if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(email)) {
      this.logger.trace('email is invalid');
      throw this.errors.badRequest('email is invalid');
    }

    const user = await this.userRepository.getUserByUsername(username);

    if (user !== undefined) {
      this.logger.trace('username is already taken');
      throw this.errors.conflict('username is already taken');
    }

    const emailCheck = await this.userRepository.getUserByEmail(email);

    if (emailCheck !== undefined) {
      this.logger.trace('email is already taken');
      throw this.errors.conflict('email is already taken');
    }

    try {
      await this.userRepository.createUser(userData);
    } catch (error) {
      this.logger.error(error);
      throw this.errors.internalServerError('Internal server error');
    }
  }
};

module.exports = UserUseCase;
